package pl.blaj.auth.session;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.model.User;

import java.sql.SQLException;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class SessionManager {

  private final Map<UUID, PlayerSession> activePlayerSessions = new ConcurrentHashMap<>();
  private final LoadingCache<UUID, PlayerSession> preloadPlayerCache;

  private final Map<UUID, Location> activePlayerLocations = new ConcurrentHashMap<>();

  public SessionManager() {
    this.preloadPlayerCache =
        CacheBuilder.newBuilder()
            .expireAfterWrite(30L, TimeUnit.SECONDS)
            .build(
                new CacheLoader<UUID, PlayerSession>() {
                  @Override
                  public PlayerSession load(UUID uuid) throws Exception {
                    return newSession(uuid);
                  }
                });
  }

  public final PlayerSession getPlayerSession(final Player player) {
    if (!player.isOnline()) {
      throw new IllegalStateException("Can not retrieve player session from offline player");
    }

    final UUID uuid = player.getUniqueId();
    final PlayerSession playerSession;

    if (activePlayerSessions.containsKey(uuid)) {
      playerSession = activePlayerSessions.get(uuid);
    } else {
      playerSession = preloadPlayerCache.getUnchecked(uuid);
      activePlayerSessions.put(uuid, playerSession);
      preloadPlayerCache.invalidate(uuid);
    }

    return playerSession;
  }

  public final PlayerSession getOfflinePlayerSession(final String playerName) {
    try {
      User user =
          Auth.getInstance()
              .getDataSource()
              .getDatabase()
              .getUserRepository()
              .findByUsername(playerName);

      if (user == null) {
        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(playerName);

        if (offlinePlayer == null || offlinePlayer.getUniqueId() == null) {
          return null;
        }

        return newSession(offlinePlayer.getUniqueId());
      }

      return new PlayerSession(user, UserState.UNAUTHENTICATED);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  public final void playerLogout(final Player player) {
    activePlayerSessions.remove(player.getUniqueId());
  }

  public final void preloadSession(final UUID playerUuid) {
    preloadPlayerCache.getUnchecked(playerUuid);
  }

  private final PlayerSession newSession(final UUID playerUuid) {
    try {
      User user =
          Auth.getInstance()
              .getDataSource()
              .getDatabase()
              .getUserRepository()
              .findByUuid(playerUuid);
      UserState userState = UserState.UNAUTHENTICATED;

      if (user == null) {
        user = this.createBlankUser(playerUuid);
        userState = UserState.UNREGISTERED;
      }

      return new PlayerSession(user, userState);
    } catch (SQLException e) {
      e.printStackTrace();
      return null;
    }
  }

  private final User createBlankUser(final UUID playerUuid) {
    User user = new User();
    user.setUuid(playerUuid.toString());

    return user;
  }

  public Map<UUID, Location> getActivePlayerLocations() {
    return activePlayerLocations;
  }
}
