package pl.blaj.auth.session;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.model.User;

import java.sql.SQLException;
import java.util.UUID;

public class PlayerSession {

  private User user;

  private UserState userState;

  private int loginAttempts;

  public PlayerSession(User user, UserState userState) {
    this(user, userState, 0);
  }

  public PlayerSession(User user, UserState userState, int loginAttempts) {
    this.user = user;
    this.userState = userState;
  }

  public void refreshUser() throws Exception {
    User newUser;

    try {
      UUID uuid = UUID.fromString(this.user.getUuid());
      newUser =
          Auth.getInstance().getDataSource().getDatabase().getUserRepository().findByUuid(uuid);
    } catch (SQLException e) {
      throw new Exception("Failed to load profile from database", e);
    }

    if (newUser != null && !this.isRegistered()) {
      throw new Exception("Profile was registered while in database");
    }

    if (newUser == null && this.isRegistered()) {
      throw new Exception("Profile was not found, even though it should be there");
    }

    if (newUser == null) {
      return;
    }

    this.user = newUser;
  }

  public Player getPlayer() {
    return Bukkit.getPlayer(user.getUsername());
  }

  public boolean isAuthorized() {
    return userState == UserState.AUTHENTICATED;
  }

  public boolean isUnauthorized() {
    return userState == UserState.UNAUTHENTICATED;
  }

  public boolean isRegistered() {
    return user.getPassword() != null;
  }

  public boolean isLogged() {
    return isAuthorized() && isRegistered();
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public UserState getUserState() {
    return userState;
  }

  public void setUserState(UserState userState) {
    this.userState = userState;
  }

  public int getLoginAttempts() {
    return loginAttempts;
  }

  public void setLoginAttempts(int loginAttempts) {
    this.loginAttempts = loginAttempts;
  }
}
