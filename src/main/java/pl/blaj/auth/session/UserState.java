package pl.blaj.auth.session;

public enum UserState {
  UNREGISTERED,
  UNAUTHENTICATED,
  AUTHENTICATED,
}
