package pl.blaj.auth.event.user;

import pl.blaj.auth.event.EssentialsEvent;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.session.PlayerSession;

public class UserBanEvent extends EssentialsEvent {

  private final PlayerSession userBanned;

  public UserBanEvent(EventCause eventCause, PlayerSession doer, PlayerSession userBanned) {
    super(eventCause, doer);
    this.userBanned = userBanned;
  }

  public UserBanEvent(
      EventCause eventCause, PlayerSession doer, PlayerSession userBanned, boolean isAsync) {
    super(eventCause, doer, isAsync);
    this.userBanned = userBanned;
  }
}
