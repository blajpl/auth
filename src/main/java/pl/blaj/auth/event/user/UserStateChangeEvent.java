package pl.blaj.auth.event.user;

import pl.blaj.auth.event.EssentialsEvent;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.session.UserState;

public class UserStateChangeEvent extends EssentialsEvent {

  private final UserState newState;

  public UserStateChangeEvent(EventCause eventCause, PlayerSession doer, UserState newState) {
    super(eventCause, doer);
    this.newState = newState;
  }

  public UserStateChangeEvent(
      EventCause eventCause, PlayerSession doer, UserState newState, boolean isAsync) {
    super(eventCause, doer, isAsync);
    this.newState = newState;
  }

  public UserState getNewState() {
    return newState;
  }
}
