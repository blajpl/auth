package pl.blaj.auth.event.user;

import pl.blaj.auth.event.EssentialsEvent;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.session.PlayerSession;

public class UserLoginEvent extends EssentialsEvent {

  public UserLoginEvent(EventCause eventCause, PlayerSession doer) {
    super(eventCause, doer);
  }

  public UserLoginEvent(EventCause eventCause, PlayerSession doer, boolean isAsync) {
    super(eventCause, doer, isAsync);
  }
}
