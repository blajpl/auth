package pl.blaj.auth.event.user;

import pl.blaj.auth.event.EssentialsEvent;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.session.PlayerSession;

public class UserRegisterEvent extends EssentialsEvent {

  public UserRegisterEvent(EventCause eventCause, PlayerSession doer) {
    super(eventCause, doer);
  }

  public UserRegisterEvent(EventCause eventCause, PlayerSession doer, boolean isAsync) {
    super(eventCause, doer, isAsync);
  }
}
