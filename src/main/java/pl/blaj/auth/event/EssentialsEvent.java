package pl.blaj.auth.event;

import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import pl.blaj.auth.session.PlayerSession;

public abstract class EssentialsEvent extends Event implements Cancellable {

  private static final HandlerList handlers = new HandlerList();

  private final EventCause eventCause;
  private final PlayerSession doer;

  private boolean cancelled = false;
  private String cancelledMessage = null;

  public EssentialsEvent(EventCause eventCause, PlayerSession doer) {
    super(true);
    this.eventCause = eventCause;
    this.doer = doer;
  }

  public EssentialsEvent(EventCause eventCause, PlayerSession doer, boolean isAsync) {
    super(isAsync);
    this.eventCause = eventCause;
    this.doer = doer;
  }

  public EventCause getEventCause() {
    return eventCause;
  }

  public PlayerSession getDoer() {
    return doer;
  }

  public String getCancelledMessage() {
    return cancelledMessage;
  }

  public void setCancelledMessage(String cancelledMessage) {
    this.cancelledMessage = cancelledMessage;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }

  @Override
  public boolean isCancelled() {
    return cancelled;
  }

  @Override
  public void setCancelled(boolean cancelled) {
    this.cancelled = cancelled;
  }

  @Override
  public HandlerList getHandlers() {
    return handlers;
  }
}
