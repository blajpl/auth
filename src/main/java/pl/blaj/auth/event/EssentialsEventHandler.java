package pl.blaj.auth.event;

import org.bukkit.Bukkit;

public class EssentialsEventHandler {

  public static boolean handle(EssentialsEvent essentialsEvent) {
    Bukkit.getPluginManager().callEvent(essentialsEvent);

    if (essentialsEvent.isCancelled()) {
      return false;
    }

    return true;
  }
}
