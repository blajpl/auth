package pl.blaj.auth.event;

public enum EventCause {
  CONSOLE,
  SYSTEM,
  USER
}
