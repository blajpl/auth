package pl.blaj.auth.util;

import javafx.util.Pair;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import pl.blaj.auth.exception.EntityNotFoundException;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

public class ConfigUtil {
  private static HashMap<String, Pair<File, FileConfiguration>> configurationMap = new HashMap<>();

  private Plugin plugin;
  private boolean autoSave;

  public ConfigUtil(Plugin plugin, boolean autoSave) {
    this.plugin = plugin;
    this.autoSave = autoSave;
  }

  public static void init(Plugin plugin, String... filenames) {
    if (!plugin.getDataFolder().exists()) {
      plugin.getDataFolder().mkdirs();

      for (String filename : filenames) {
        plugin.saveResource(filename + ".yml", false);
      }
    }
  }

  public boolean register(String filename) {
    if (configurationMap.containsKey(filename)) {
      return false;
    }

    File file = new File(plugin.getDataFolder(), filename + ".yml");

    if (file.exists()) {
      FileConfiguration fileConfiguration = new YamlConfiguration();

      try {
        fileConfiguration.load(file);
        configurationMap.put(filename, new Pair<>(file, fileConfiguration));
        return true;
      } catch (IOException | InvalidConfigurationException e) {
        e.printStackTrace();
        return false;
      }
    }

    return false;
  }

  public Object getField(String configName, String path) {
    try {
      this.validateConfigExists(configName);
      this.validatePathExists(configName, path);

      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();

      return fileConfiguration.get(path);
    } catch (EntityNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  public List<?> getList(String configName, String path) {
    try {
      this.validateConfigExists(configName);
      this.validatePathExists(configName, path);

      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();

      return fileConfiguration.getList(path);
    } catch (EntityNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  public ConfigurationSection getConfigurationSection(String configName, String path)
      throws EntityNotFoundException {
    try {
      this.validateConfigExists(configName);
      this.validatePathExists(configName, path);

      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();

      return fileConfiguration.getConfigurationSection(path);
    } catch (EntityNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  public Set<String> getKeys(String configName, boolean deep) {
    try {
      this.validateConfigExists(configName);

      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();

      return fileConfiguration.getKeys(deep);
    } catch (EntityNotFoundException e) {
      e.printStackTrace();
    }

    return null;
  }

  public void setField(String configName, String path, Object overwrite)
      throws EntityNotFoundException {

    try {
      this.validateConfigExists(configName);
      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();
      fileConfiguration.set(path, overwrite);

      if (this.autoSave) {
        this.save(configName);
      }
    } catch (EntityNotFoundException | IOException e) {
      e.printStackTrace();
    }
  }

  public void setList(String configName, String path, List<Object> overwriteList)
      throws EntityNotFoundException {

    try {
      this.validateConfigExists(configName);
      FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();
      fileConfiguration.set(path, overwriteList);

      if (this.autoSave) {
        this.save(configName);
      }
    } catch (EntityNotFoundException | IOException e) {
      e.printStackTrace();
    }
  }

  public void save(String configName) throws EntityNotFoundException, IOException {
    this.validateConfigExists(configName);
    FileConfiguration fileConfiguration = configurationMap.get(configName).getValue();
    fileConfiguration.save(configurationMap.get(configName).getKey());
  }

  private void validateConfigExists(String configName) throws EntityNotFoundException {
    if (!configExists(configName)) {
      throw new EntityNotFoundException("Invalid config file: " + configName);
    }
  }

  private void validatePathExists(String configName, String path) throws EntityNotFoundException {
    if (!pathExists(configName, path)) {
      throw new EntityNotFoundException("Invalid path: " + path);
    }
  }

  private boolean configExists(String configName) {
    return configurationMap.containsKey(configName);
  }

  private boolean pathExists(String configName, String path) {
    return configExists(configName) && configurationMap.get(configName).getValue().contains(path);
  }
}
