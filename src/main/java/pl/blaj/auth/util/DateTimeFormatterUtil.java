package pl.blaj.auth.util;

import java.time.format.DateTimeFormatter;

public class DateTimeFormatterUtil {

  public static DateTimeFormatter DEFAULT_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
  public static DateTimeFormatter DAY_FORMAT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
  public static DateTimeFormatter HOUR_FORMAT = DateTimeFormatter.ofPattern("HH:mm");
}
