package pl.blaj.auth.util;

import org.bukkit.ChatColor;

import java.util.ArrayList;
import java.util.List;

public final class ChatUtil {

  public static String colored(String message) {
    return message != null ? ChatColor.translateAlternateColorCodes('&', message) : null;
  }

  public static List<String> colored(List<String> messages) {
    final List<String> colored = new ArrayList<>();
    messages.forEach(message -> colored.add(message));
    return colored;
  }
}
