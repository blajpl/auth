package pl.blaj.auth.database.model;

public class User {

  private int id;

  private String uuid;

  private String username;

  private String password;

  // TODO: rename this!
  private Integer beforeLoginLocationId;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUuid() {
    return uuid;
  }

  public void setUuid(String uuid) {
    this.uuid = uuid;
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public Integer getBeforeLoginLocationId() {
    return beforeLoginLocationId;
  }

  public void setBeforeLoginLocationId(Integer beforeLoginLocationId) {
    this.beforeLoginLocationId = beforeLoginLocationId;
  }
}
