package pl.blaj.auth.database.model;

import java.time.LocalDateTime;

public class UserBan {

  private int id;

  private String reason;

  private LocalDateTime startBan;

  private LocalDateTime endBan;

  private User user;

  private User admin;

  private boolean unbanned;

  private User unbannedBy;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getReason() {
    return reason;
  }

  public void setReason(String reason) {
    this.reason = reason;
  }

  public LocalDateTime getStartBan() {
    return startBan;
  }

  public void setStartBan(LocalDateTime startBan) {
    this.startBan = startBan;
  }

  public LocalDateTime getEndBan() {
    return endBan;
  }

  public void setEndBan(LocalDateTime endBan) {
    this.endBan = endBan;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public User getAdmin() {
    return admin;
  }

  public void setAdmin(User admin) {
    this.admin = admin;
  }

  public boolean isUnbanned() {
    return unbanned;
  }

  public void setUnbanned(boolean unbanned) {
    this.unbanned = unbanned;
  }

  public User getUnbannedBy() {
    return unbannedBy;
  }

  public void setUnbannedBy(User unbannedBy) {
    this.unbannedBy = unbannedBy;
  }
}
