package pl.blaj.auth.database.common;

public class Page {

    public static final Page DEFAULT = new Page().setPageNo(0).setPageCount(10);

    private int pageNo;

    private int pageCount;

    public int getPageNo() {
        return pageNo;
    }

    public Page setPageNo(int pageNo) {
        this.pageNo = pageNo;
        return this;
    }

    public int getPageCount() {
        return pageCount;
    }

    public Page setPageCount(int pageCount) {
        this.pageCount = pageCount;
        return this;
    }

}
