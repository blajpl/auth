package pl.blaj.auth.database.common;

public class Pagination {

    public static Pagination DEFAULT = new Pagination().setPage(Page.DEFAULT);

    private Page page;

    public Page getPage() {
        return page;
    }

    public Pagination setPage(Page page) {
        this.page = page;
        return this;
    }
}
