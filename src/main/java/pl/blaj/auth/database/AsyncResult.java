package pl.blaj.auth.database;

public class AsyncResult<T> {

  private final boolean success;
  private final T result;
  private final Exception error;

  public AsyncResult(boolean success, T result, Exception error) {
    this.success = success;
    this.result = result;
    this.error = error;
  }

  public boolean isSuccess() {
    return success;
  }

  public T getResult() {
    return result;
  }

  public Exception getError() {
    return error;
  }
}
