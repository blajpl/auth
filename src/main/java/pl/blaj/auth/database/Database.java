package pl.blaj.auth.database;

import pl.blaj.auth.Auth;
import pl.blaj.auth.database.repository.UserBanRepository;
import pl.blaj.auth.database.repository.UserLocationRepository;
import pl.blaj.auth.database.repository.UserRepository;

import javax.sql.DataSource;

// TODO: should be named PersistenceManager?
public class Database {

  private final UserRepository userRepository;
  private final UserLocationRepository userLocationRepository;
  private final UserBanRepository userBanRepository;

  public Database(Auth plugin, DataSource dataSource) {
    this.userRepository = new UserRepository(plugin, dataSource);
    this.userLocationRepository = new UserLocationRepository(plugin, dataSource);
    this.userBanRepository = new UserBanRepository(plugin, dataSource);
  }

  public UserRepository getUserRepository() {
    return userRepository;
  }

  public UserLocationRepository getUserLocationRepository() {
    return userLocationRepository;
  }

  public UserBanRepository getUserBanRepository() {
    return userBanRepository;
  }
}
