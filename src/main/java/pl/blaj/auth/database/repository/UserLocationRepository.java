package pl.blaj.auth.database.repository;

import org.bukkit.Bukkit;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.AsyncResult;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.database.model.UserLocation;

import javax.sql.DataSource;
import java.sql.*;
import java.util.function.Consumer;

public class UserLocationRepository {

  private final Auth auth;
  private final DataSource dataSource;

  public UserLocationRepository(Auth auth, DataSource dataSource) {
    this.auth = auth;
    this.dataSource = dataSource;
  }

  public void insert(
      User user, UserLocation userLocation, Consumer<AsyncResult<UserLocation>> callback) {
    Bukkit.getScheduler()
        .runTaskAsynchronously(
            auth,
            () -> {
              try {
                this.insertBlock(user, userLocation);
                this.resolveResult(callback, userLocation);
              } catch (SQLException e) {
                this.resolveError(callback, e);
              }
            });
  }

  public void insertBlock(User user, UserLocation userLocation) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement =
          "INSERT INTO location(world, x, y, z, yaw, pitch) VALUES (?, ?, ?, ?, ?, ?)";

      try (PreparedStatement preparedStatement =
          connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setString(1, userLocation.getWorld());
        preparedStatement.setDouble(2, userLocation.getX());
        preparedStatement.setDouble(3, userLocation.getY());
        preparedStatement.setDouble(4, userLocation.getZ());
        preparedStatement.setFloat(5, userLocation.getYaw());
        preparedStatement.setFloat(6, userLocation.getPitch());

        preparedStatement.execute();

        try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
          if (!generatedKeys.next()) {
            throw new RuntimeException(
                "No keys were returned after insert! Check if id column has autoincrement!");
          }

          userLocation.setId(generatedKeys.getInt(1));
        }
      }

      user.setBeforeLoginLocationId(userLocation.getId());

      // TODO: wtf?? not mix 2 repositories!!!
      statement = "UPDATE user SET location_id = ? WHERE id = ?";
      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setInt(1, userLocation.getId());
        preparedStatement.setInt(2, user.getId());

        if (preparedStatement.executeUpdate() < 1) {
          throw new SQLException("Failed to set location id in user");
        }
      }
    }
  }

  public void findById(int id, Consumer<AsyncResult<UserLocation>> callback) {
    Bukkit.getScheduler()
        .runTaskAsynchronously(
            auth,
            () -> {
              try {
                final UserLocation userLocation = findByIdBlock(id);
                this.resolveResult(callback, userLocation);
              } catch (SQLException e) {
                this.resolveError(callback, e);
              }
            });
  }

  public UserLocation findByIdBlock(int id) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM location WHERE id = ?";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setInt(1, id);

        try (ResultSet result = preparedStatement.executeQuery()) {
          if (!result.next()) {
            return null;
          }

          return map(result);
        }
      }
    }
  }

  private UserLocation map(ResultSet resultSet) throws SQLException {
    UserLocation userLocation = new UserLocation();

    userLocation.setId(resultSet.getInt("id"));
    userLocation.setX(resultSet.getDouble("x"));
    userLocation.setY(resultSet.getDouble("y"));
    userLocation.setZ(resultSet.getDouble("z"));
    userLocation.setYaw(resultSet.getFloat("yaw"));
    userLocation.setPitch(resultSet.getFloat("pitch"));

    return userLocation;
  }

  private <T> void resolveResult(Consumer<AsyncResult<T>> callback, T result) {
    Bukkit.getScheduler()
        .runTask(
            auth,
            () -> {
              callback.accept(new AsyncResult<T>(true, result, null));
            });
  }

  private <T> void resolveError(Consumer<AsyncResult<T>> callback, Exception error) {
    Bukkit.getScheduler()
        .runTask(
            auth,
            () -> {
              callback.accept(new AsyncResult<T>(false, null, error));
            });
  }
}
