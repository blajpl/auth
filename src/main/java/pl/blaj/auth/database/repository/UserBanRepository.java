package pl.blaj.auth.database.repository;

import org.bukkit.Bukkit;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.AsyncResult;
import pl.blaj.auth.database.common.Pagination;
import pl.blaj.auth.database.common.PaginationList;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.database.model.UserBan;

import javax.sql.DataSource;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class UserBanRepository {

  private final Auth auth;
  private final DataSource dataSource;

  public UserBanRepository(Auth auth, DataSource dataSource) {
    this.auth = auth;
    this.dataSource = dataSource;
  }

  public List<UserBan> findAll() throws SQLException {
    List<UserBan> userBanList = new ArrayList<>();

    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM ban ORDER BY id ASC";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        try (ResultSet result = preparedStatement.executeQuery()) {
          while (result.next()) {
            UserBan userBan = this.map(result);
            userBanList.add(userBan);
          }
        }
      }
    }

    return userBanList;
  }

  public PaginationList<UserBan> findAllByPagination(Pagination pagination) throws SQLException {
    PaginationList<UserBan> userBanPaginationList = new PaginationList<>();
    List<UserBan> userBanList = new ArrayList<>();

    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM ban ORDER BY id ASC LIMIT ? OFFSET ?";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setInt(1, pagination.getPage().getPageCount());
        preparedStatement.setInt(2, pagination.getPage().getPageNo());
        try (ResultSet result = preparedStatement.executeQuery()) {
          while (result.next()) {
            UserBan userBan = this.map(result);
            userBanList.add(userBan);
          }
        }
      }
    }

    userBanPaginationList.setItems(userBanList);
    userBanPaginationList.setItemsCount(userBanList.size());
    userBanPaginationList.setPage(pagination.getPage());

    return userBanPaginationList;
  }

  public int countAll() throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT COUNT(*) as count FROM ban";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        try (ResultSet result = preparedStatement.executeQuery()) {
          if (result.next()) {
            return result.getInt("count");
          }
        }
      }
    }

    return 0;
  }

  public void insert(User user, UserBan userBan, User userAdmin) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement =
          "INSERT INTO ban(reason, start_ban, end_ban, user_id, admin_id) VALUES (?, ?, ?, ?, ?)";

      try (PreparedStatement preparedStatement =
          connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setString(1, userBan.getReason());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(userBan.getStartBan()));
        preparedStatement.setTimestamp(3, Timestamp.valueOf(userBan.getEndBan()));
        preparedStatement.setInt(4, user.getId());
        preparedStatement.setInt(5, userAdmin.getId());

        preparedStatement.execute();

        try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
          if (!generatedKeys.next()) {
            throw new RuntimeException(
                "No keys were returned after insert! Check if id column has autoincrement!");
          }

          userBan.setId(generatedKeys.getInt(1));
        }
      }
    }
  }

  public UserBan findOneByUserOnDateTime(User user, LocalDateTime onDateTime) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement =
          "SELECT * FROM ban WHERE user_id = ? AND (? >= start_ban AND ? <= end_ban)";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setInt(1, user.getId());
        preparedStatement.setTimestamp(2, Timestamp.valueOf(onDateTime));
        preparedStatement.setTimestamp(3, Timestamp.valueOf(onDateTime));

        try (ResultSet result = preparedStatement.executeQuery()) {
          if (!result.next()) {
            return null;
          }

          return map(result);
        }
      }
    }
  }

  public void update(UserBan userBan) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "UPDATE ban SET unbanned = ?, unbanned_by = ? WHERE id = ?";

      try (PreparedStatement preparedStatement =
                   connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setBoolean(1, userBan.isUnbanned());
        preparedStatement.setInt(2, userBan.getUser().getId());
        preparedStatement.setInt(3, userBan.getId());

        preparedStatement.execute();
      }
    }
  }

  private UserBan map(ResultSet resultSet) throws SQLException {
    UserBan userBan = new UserBan();

    userBan.setId(resultSet.getInt(1));
    userBan.setReason(resultSet.getString(2));
    userBan.setStartBan(resultSet.getTimestamp(3).toLocalDateTime());
    userBan.setEndBan(resultSet.getTimestamp(4).toLocalDateTime());
    userBan.setUser(this.fetchUser(resultSet.getInt(5)));
    userBan.setAdmin(this.fetchUser(resultSet.getInt(6)));
    userBan.setUnbanned(resultSet.getBoolean(7));
    userBan.setUnbannedBy(this.fetchUser(resultSet.getInt(8)));

    return userBan;
  }

  private User fetchUser(int userId) throws SQLException {
    return Auth.getInstance().getDataSource().getDatabase().getUserRepository().findById(userId);
  }

  private <T> void resolveResult(Consumer<AsyncResult<T>> callback, T result) {
    Bukkit.getScheduler()
        .runTask(
            auth,
            () -> {
              callback.accept(new AsyncResult<T>(true, result, null));
            });
  }

  private <T> void resolveError(Consumer<AsyncResult<T>> callback, Exception error) {
    Bukkit.getScheduler()
        .runTask(
            auth,
            () -> {
              callback.accept(new AsyncResult<T>(false, null, error));
            });
  }
}
