package pl.blaj.auth.database.repository;

import pl.blaj.auth.Auth;
import pl.blaj.auth.database.model.User;

import javax.sql.DataSource;
import java.sql.*;
import java.util.UUID;

public class UserRepository {

  private final Auth auth;
  private final DataSource dataSource;

  public UserRepository(Auth auth, DataSource dataSource) {
    this.auth = auth;
    this.dataSource = dataSource;
  }

  public void insert(User user) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "INSERT INTO user(uuid, username, password) VALUES (?, ?, ?)";

      try (PreparedStatement preparedStatement =
          connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setString(1, user.getUuid());
        preparedStatement.setString(2, user.getUsername());
        preparedStatement.setString(3, user.getPassword());

        preparedStatement.execute();

        try (ResultSet generatedKeys = preparedStatement.getGeneratedKeys()) {
          if (!generatedKeys.next()) {
            throw new RuntimeException(
                "No keys were returned after insert! Check if id column has autoincrement!");
          }

          user.setId(generatedKeys.getInt(1));
        }
      }
    }
  }

  public void update(User user) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "UPDATE user SET username = ?, password = ? WHERE id = ?";

      try (PreparedStatement preparedStatement =
          connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setString(1, user.getUsername());
        preparedStatement.setString(2, user.getPassword());
        preparedStatement.setInt(3, user.getId());

        preparedStatement.execute();
      }
    }
  }

  public void updatePassword(User user) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "UPDATE user SET password = ? WHERE id = ?";

      try (PreparedStatement preparedStatement =
          connection.prepareStatement(statement, Statement.RETURN_GENERATED_KEYS)) {
        preparedStatement.setString(1, user.getPassword());
        preparedStatement.setInt(2, user.getId());

        preparedStatement.execute();
      }
    }
  }

  public User findByUuid(UUID uuid) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM user WHERE uuid = ?";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setString(1, uuid.toString());

        try (ResultSet result = preparedStatement.executeQuery()) {
          if (!result.next()) {
            return null;
          }

          return map(result);
        }
      }
    }
  }

  public User findById(int id) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM user WHERE id = ?";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setInt(1, id);

        try (ResultSet result = preparedStatement.executeQuery()) {
          if (!result.next()) {
            return null;
          }

          return map(result);
        }
      }
    }
  }

  public User findByUsername(String username) throws SQLException {
    try (Connection connection = dataSource.getConnection()) {
      String statement = "SELECT * FROM user WHERE username = ?";

      try (PreparedStatement preparedStatement = connection.prepareStatement(statement)) {
        preparedStatement.setString(1, username);

        try (ResultSet result = preparedStatement.executeQuery()) {
          if (!result.next()) {
            return null;
          }

          return map(result);
        }
      }
    }
  }

  private User map(ResultSet result) throws SQLException {
    User user = new User();

    user.setId(result.getInt("id"));
    user.setUuid(result.getString("uuid"));
    user.setUsername(result.getString("username"));
    user.setPassword(result.getString("password"));
    user.setBeforeLoginLocationId(result.getInt("location_id"));

    return user;
  }
}
