package pl.blaj.auth.database.datasource;

import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitTask;

import javax.sql.*;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

public class SingleConnectionDataSource extends DataSourceAdapter
    implements ConnectionEventListener, Runnable {

  private static final int VALID_CHECK_BYPASS = 500;
  private static final int DEFAULT_TIMEOUT = 5000;
  private static final long DEFAULT_MAX_LIFETIME = TimeUnit.MINUTES.toMillis(30);

  private final Plugin plugin;
  private final ConnectionPoolDataSource dataSource;
  private final ReentrantLock lock;

  private final int timeout;
  private final long maxLifetime;

  private volatile PooledConnection pooledConnection;
  private volatile long lastUsedTime;
  private volatile boolean obtainingConnection;

  private BukkitTask recreateTask;
  private boolean closeConnection = false;

  public SingleConnectionDataSource(Plugin plugin, ConnectionPoolDataSource dataSource) {
    this(plugin, dataSource, DEFAULT_TIMEOUT, DEFAULT_MAX_LIFETIME);
  }

  public SingleConnectionDataSource(
      Plugin plugin, ConnectionPoolDataSource dataSource, int timeout, long maxLifetime) {
    this.plugin = plugin;
    this.dataSource = dataSource;
    this.lock = new ReentrantLock(true);

    this.timeout = timeout;
    this.maxLifetime = maxLifetime;
  }

  @Override
  public Connection getConnection() throws SQLException {
    if (closeConnection) {
      throw new SQLException("Database is shutting down");
    }

    lock.lock();
    this.obtainingConnection = true;

    try {
      if (pooledConnection != null) {
        try {
          final Connection connection = pooledConnection.getConnection();

          if (!connection.isClosed()) {
            if ((lastUsedTime - System.currentTimeMillis() <= VALID_CHECK_BYPASS)
                || connection.isValid(timeout)) {
              this.obtainingConnection = false;
              return connection;
            } else {
              tryCloseConnection(pooledConnection);
            }
          }
        } catch (SQLException e) {
          tryCloseConnection(pooledConnection);
        }
      }

      createConnection();
      Connection connection = pooledConnection.getConnection();
      this.obtainingConnection = false;
      return connection;
    } catch (Throwable t) {
      lock.unlock();
      throw t;
    }
  }

  public void createConnection() throws SQLException {
    if (recreateTask != null) {
      recreateTask.cancel();
    }

    if (pooledConnection != null) {
      tryCloseConnection(pooledConnection);
    }

    this.pooledConnection = dataSource.getPooledConnection();
    pooledConnection.addConnectionEventListener(this);

    this.recreateTask =
        Bukkit.getScheduler().runTaskLaterAsynchronously(plugin, this, maxLifetime / 50);
  }

  @Override
  public void run() {
    this.recreateTask = null;
    lock.lock();

    try {
      tryCloseConnection(pooledConnection);
      createConnection();
    } catch (SQLException e) {
      e.printStackTrace();
    } finally {
      lock.unlock();
    }
  }

  @Override
  public void connectionClosed(ConnectionEvent connectionEvent) {
    this.lastUsedTime = System.currentTimeMillis();

    if (!obtainingConnection) {
      lock.unlock();
    }
  }

  @Override
  public void connectionErrorOccurred(ConnectionEvent connectionEvent) {
    final PooledConnection brokenConnection =
        connectionEvent.getSource() instanceof PooledConnection
            ? (PooledConnection) connectionEvent.getSource()
            : pooledConnection;

    this.pooledConnection = null;

    if (!obtainingConnection) {
      lock.unlock();
    }

    tryCloseConnection(brokenConnection);
  }

  public void shutdown() throws SQLException {
    this.closeConnection = true;

    if (recreateTask != null) {
      recreateTask.cancel();
    }

    lock.lock();

    if (pooledConnection != null) {
      pooledConnection.close();
      this.pooledConnection = null;
    }
  }

  private void tryCloseConnection(PooledConnection connection) {
    this.pooledConnection = null;

    if (connection == null) {
      return;
    }

    try {
      connection.close();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
