package pl.blaj.auth.database.datasource;

import com.mysql.cj.jdbc.MysqlConnectionPoolDataSource;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.Database;

import javax.sql.ConnectionPoolDataSource;
import java.sql.SQLException;

public class DataSource {

  private Auth plugin;

  private SingleConnectionDataSource dataSource;
  private Database database;

  public DataSource(Auth plugin) {
    this.plugin = plugin;
  }

  public void enable() {
    ConnectionPoolDataSource dataSourceConfig;

    boolean useMysql = (boolean) Auth.getConfiguration().getField("database", "use-mysql");

    if (useMysql) {
      dataSourceConfig = this.mysqlConnection();
    } else {
      dataSourceConfig = this.sqliteConnection();
    }

    this.dataSource = new SingleConnectionDataSource(this.plugin, dataSourceConfig);
    this.database = new Database(this.plugin, this.dataSource);

    try {
      this.dataSource.createConnection();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public void disable() {
    try {
      if (this.dataSource == null) {
        return;
      }

      this.dataSource.shutdown();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  public ConnectionPoolDataSource mysqlConnection() {
    String host = (String) Auth.getConfiguration().getField("database", "mysql-host");
    int port = (int) Auth.getConfiguration().getField("database", "mysql-port");
    String username = (String) Auth.getConfiguration().getField("database", "mysql-username");
    String password = (String) Auth.getConfiguration().getField("database", "mysql-password");
    String databaseName = (String) Auth.getConfiguration().getField("database", "mysql-name");

    MysqlConnectionPoolDataSource mysqlConfig = new MysqlConnectionPoolDataSource();
    mysqlConfig.setUrl("jdbc:mysql://" + host + ":" + port + "/" + databaseName);
    mysqlConfig.setUser(username);
    mysqlConfig.setPassword(password);
    return mysqlConfig;
  }

  public ConnectionPoolDataSource sqliteConnection() {
    return null;
  }

  public Database getDatabase() {
    return database;
  }
}
