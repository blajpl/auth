package pl.blaj.auth;

import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import pl.blaj.auth.command.misc.Commands;
import pl.blaj.auth.database.datasource.DataSource;
import pl.blaj.auth.listener.*;
import pl.blaj.auth.session.SessionManager;
import pl.blaj.auth.task.TaskManager;
import pl.blaj.auth.util.ConfigUtil;

public class Auth extends JavaPlugin {

  // tODO: add logger
  // TODO: add optional support!!!
  // TODO: in executor add check userstate!

  // TODO: ban list, unban
  // TODO: add afk check
  // TODO: add captcha to login!
  // tODO: add home command

  private static Auth auth;

  private static ConfigUtil configuration;

  private DataSource dataSource;
  private SessionManager sessionManager;

  private TaskManager threadingManager;

  @Override
  public void onLoad() {
    auth = this;

    ConfigUtil.init(this, "database", "message", "config");

    configuration = new ConfigUtil(this, true);
    configuration.register("database");
    configuration.register("message");
    configuration.register("config");

    Commands commands = new Commands();
    commands.register();

    threadingManager = new TaskManager();
  }

  @Override
  public void onEnable() {
    auth = this;

    this.dataSource = new DataSource(this);
    this.dataSource.enable();

    this.sessionManager = new SessionManager();

    this.threadingManager.enable();

    PluginManager pluginManager = Bukkit.getPluginManager();
    pluginManager.registerEvents(new PlayerListener(), this);
    pluginManager.registerEvents(new UserListener(), this);
  }

  @Override
  public void onDisable() {
    this.dataSource.disable();
    Bukkit.getScheduler().cancelTasks(this);

    auth = null;
  }

  public static Auth getInstance() {
    return auth;
  }

  public static ConfigUtil getConfiguration() {
    return configuration;
  }

  public DataSource getDataSource() {
    return dataSource;
  }

  public SessionManager getSessionManager() {
    return sessionManager;
  }
}
