package pl.blaj.auth.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import pl.blaj.auth.event.user.UserBanEvent;
import pl.blaj.auth.event.user.UserLoginEvent;
import pl.blaj.auth.event.user.UserLogoutEvent;
import pl.blaj.auth.event.user.UserRegisterEvent;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.session.UserState;

public class UserListener implements Listener {

  @EventHandler
  public void onUserLogin(UserLoginEvent event) {
    PlayerSession playerSession = event.getDoer();
    playerSession.setUserState(UserState.AUTHENTICATED);
  }

  @EventHandler
  public void onUserLogout(UserLogoutEvent event) {
    PlayerSession playerSession = event.getDoer();
    playerSession.setUserState(UserState.UNAUTHENTICATED);
  }

  @EventHandler
  public void onUserRegister(UserRegisterEvent event) {
    PlayerSession playerSession = event.getDoer();
    playerSession.setUserState(UserState.AUTHENTICATED);
  }

  @EventHandler
  public void onUserBan(UserBanEvent event) {}
}
