package pl.blaj.auth.listener;

import com.google.common.collect.Lists;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.*;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import pl.blaj.auth.Auth;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.database.model.UserBan;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.database.model.UserLocation;
import pl.blaj.auth.util.ChatUtil;
import pl.blaj.auth.util.DateTimeFormatterUtil;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class PlayerListener implements Listener {

  private final List<String> ALLOWED_COMMANDS = Lists.newArrayList("/login", "/register", "/forgot-password");
  private final String ALLOWED_PLAYER_NAME_LETTERS = "[a-zA-Z0-9_]+";

  @EventHandler
  public void onPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
    for (Player player : Bukkit.getOnlinePlayers()) {
      if (player.getName().equalsIgnoreCase(event.getName())) {
        PlayerSession playerSession =
            Auth.getInstance().getSessionManager().getPlayerSession(player);
        if (playerSession.isAuthorized()) {
          event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
          event.setKickMessage(ChatUtil.colored("&cJesteś już zalogowany!"));
          return;
        }
      }
    }

    final String playerName = event.getName();

    if (playerName.length() < 2) {
      event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
      event.setKickMessage(ChatUtil.colored("&cNick jest za krótki!"));
      return;
    }

    if (playerName.length() > 16) {
      event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
      event.setKickMessage(ChatUtil.colored("&cNick jest za długi!"));
      return;
    }

    if (!playerName.matches(ALLOWED_PLAYER_NAME_LETTERS)) {
      event.setLoginResult(AsyncPlayerPreLoginEvent.Result.KICK_OTHER);
      event.setKickMessage(ChatUtil.colored("&cNick zawiera niedozwolone znaki!"));
      return;
    }

    Auth.getInstance().getSessionManager().preloadSession(event.getUniqueId());
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerLogin(PlayerLoginEvent event) {
    final Player player = event.getPlayer();
    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getOfflinePlayerSession(player.getName());
    final User user = playerSession.getUser();

    if (user == null) {
      return;
    }

    try {
      final LocalDateTime currentDateTime = LocalDateTime.now();
      final UserBan userBan =
          Auth.getInstance()
              .getDataSource()
              .getDatabase()
              .getUserBanRepository()
              .findOneByUserOnDateTime(user, currentDateTime);

      if (userBan != null && !userBan.isUnbanned()) {
        event.disallow(
            PlayerLoginEvent.Result.KICK_BANNED,
            ChatUtil.colored(
                "&cZostałeś zbanowany z powodu "
                    + userBan.getReason()
                    + " do "
                    + userBan.getEndBan().format(DateTimeFormatterUtil.DEFAULT_FORMAT)));
      }
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerJoin(PlayerJoinEvent event) {
    final Player player = event.getPlayer();

    if (isInvalidPlayer(player)) {
      return;
    }

    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);
    final User user = playerSession.getUser();

    if (user.getUsername() == null || !player.getName().equals(user.getUsername())) {
      user.setUsername(player.getName());

      if (playerSession.isRegistered()) {
        Bukkit.getScheduler()
            .runTaskAsynchronously(
                Auth.getInstance(),
                () -> {
                  try {
                    Auth.getInstance()
                        .getDataSource()
                        .getDatabase()
                        .getUserRepository()
                        .insert(user);
                  } catch (SQLException e) {
                    // TODO:: log this
                  }
                });
      }
    }

    if (playerSession.isAuthorized() || !playerSession.isRegistered()) {
      return;
    }

    player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1));

    if (user.getBeforeLoginLocationId() == null) {
      final Location originLocation = player.getLocation().clone();
      final UserLocation userLocation = new UserLocation(originLocation);

      player.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());

      Bukkit.getScheduler()
          .runTaskAsynchronously(
              Auth.getInstance(),
              () -> {
                try {
                  Auth.getInstance()
                      .getDataSource()
                      .getDatabase()
                      .getUserLocationRepository()
                      .insertBlock(user, userLocation);
                  player.teleport(originLocation);
                } catch (Exception e) {
                  e.printStackTrace();
                }
              });
    }
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerQuit(PlayerQuitEvent event) {
    final Player player = event.getPlayer();
    Auth.getInstance().getSessionManager().playerLogout(player);
  }

  @EventHandler(priority = EventPriority.MONITOR)
  public void onPlayerKick(PlayerKickEvent event) {
    final Player player = event.getPlayer();
    Auth.getInstance().getSessionManager().playerLogout(player);
  }

  @EventHandler(priority = EventPriority.LOWEST)
  public void onPlayerCommandPreprocess(PlayerCommandPreprocessEvent event) {
    final Player player = event.getPlayer();

    if (isInvalidPlayer(player)) {
      return;
    }

    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);

    if (playerSession.isAuthorized()) {
      return;
    }

    final String eventCommand = event.getMessage().toLowerCase();

    for (String command : ALLOWED_COMMANDS) {
      if (eventCommand.startsWith(command)) {
        return;
      }
    }

    event.setCancelled(true);
  }

  @EventHandler
  public void onInventoryOpen(InventoryOpenEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onBlockBreak(BlockBreakEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onBlockPlace(BlockPlaceEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onPlayerDropItem(PlayerDropItemEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onPlayerChat(AsyncPlayerChatEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onPlayerMove(PlayerMoveEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onPlayerInteract(PlayerInteractEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onPlayerInteractEntity(PlayerInteractEntityEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onFoodLevelChange(FoodLevelChangeEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onEntityDamage(EntityDamageEvent event) {
    defaultEventAction(event);
  }

  @EventHandler
  public void onTarget(EntityTargetEvent event) {
    defaultEventAction(event);
  }

  private void defaultEventAction(Event event) {
    if (!(event instanceof Cancellable)) {
      return;
    }

    Player player = null;

    if (event instanceof PlayerEvent) {
      player = ((PlayerEvent) event).getPlayer();
    }

    if (event instanceof InventoryOpenEvent) {
      player = (Player) ((InventoryOpenEvent) event).getPlayer();
    }

    if (event instanceof BlockBreakEvent) {
      player = ((BlockBreakEvent) event).getPlayer();
    }

    if (event instanceof BlockPlaceEvent) {
      player = ((BlockPlaceEvent) event).getPlayer();
    }

    if (event instanceof PlayerDropItemEvent) {
      player = ((PlayerDropItemEvent) event).getPlayer();
    }

    if (event instanceof FoodLevelChangeEvent) {
      player = (Player) ((FoodLevelChangeEvent) event).getEntity();
    }

    if (event instanceof EntityDamageEvent) {
      EntityDamageEvent subEvent = (EntityDamageEvent) event;

      if (subEvent.getEntityType() != EntityType.PLAYER) {
        return;
      }

      player = (Player) ((EntityDamageEvent) event).getEntity();
    }

    if (event instanceof EntityTargetEvent) {
      EntityTargetEvent subEvent = (EntityTargetEvent) event;

      if (!(subEvent.getTarget() instanceof Player)) {
        return;
      }

      player = (Player) ((EntityTargetEvent) event).getTarget();

      if (!player.isOnline()) {
        return;
      }
    }

    if (isInvalidPlayer(player)) {
      return;
    }

    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);

    if (playerSession.isAuthorized()) {
      return;
    }

    ((Cancellable) event).setCancelled(true);
  }

  private boolean isInvalidPlayer(HumanEntity entity) {
    if (!(entity instanceof Player)) {
      return true;
    }

    final Player player = (Player) entity;

    return player.hasMetadata("NPC") || !player.isOnline();
  }
}
