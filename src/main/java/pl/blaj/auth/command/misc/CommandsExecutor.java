package pl.blaj.auth.command.misc;

import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.util.ChatUtil;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class CommandsExecutor implements CommandExecutor, TabExecutor {

  private static final List<CommandsExecutor> mainCommandsExecutors = new ArrayList<>();

  private Executor executor;
  private String permission;
  private String command;
  private boolean playerOnly;
  private boolean enabled;
  private int minArgs;

  public CommandsExecutor(
      Executor executor,
      String permission,
      String command,
      boolean enabled,
      boolean playerOnly,
      int minArgs) {
    if (executor == null || command == null) {
      return;
    }

    this.executor = executor;
    this.permission = permission;
    this.command = command;
    this.enabled = enabled;
    this.playerOnly = playerOnly;
    this.minArgs = minArgs;

    this.register();
    mainCommandsExecutors.add(this);
  }

  private void register() {
    try {
      Performer performer = new Performer(this.command);
      performer.setCommandsExecutor(this);

      performer.setPermissionMessage(ChatUtil.colored("&cNie masz uprawnień!"));
      Field field = Bukkit.getServer().getClass().getDeclaredField("commandMap");
      field.setAccessible(true);

      CommandMap commandMap = (CommandMap) field.get(Bukkit.getServer());
      commandMap.register(Auth.getInstance().getName(), performer);
    } catch (NoSuchFieldException | IllegalAccessException e) {
      e.printStackTrace();
    }
  }

  @Override
  public boolean onCommand(
      CommandSender commandSender, Command command, String label, String[] args) {
    if (!this.enabled) {
      return true;
    }

    if (this.playerOnly && !(commandSender instanceof Player)) {
      return true;
    }

    if (commandSender instanceof Player) {
      if (this.permission != null && !commandSender.hasPermission(this.permission)) {
        commandSender.sendMessage(ChatUtil.colored("&cNie masz uprawnień do tej komendy!"));
        return true;
      }
    }

    if (this.minArgs > args.length) {
      commandSender.sendMessage(ChatUtil.colored("&cNiepoprawne użycie komendy!"));
      return true;
    }

    this.executor.execute(commandSender, args);

    return true;
  }

  @Override
  public List<String> onTabComplete(
      CommandSender commandSender, Command command, String s, String[] strings) {
    return null;
  }
}
