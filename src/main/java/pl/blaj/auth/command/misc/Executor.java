package pl.blaj.auth.command.misc;

import org.bukkit.command.CommandSender;

public interface Executor {

  void execute(CommandSender commandSender, String[] args);
}
