package pl.blaj.auth.command.misc;

import org.bukkit.command.CommandSender;
import org.bukkit.command.defaults.BukkitCommand;

public class Performer extends BukkitCommand {

  private CommandsExecutor commandsExecutor;

  protected Performer(String command) {
    super(command);
  }

  @Override
  public boolean execute(CommandSender commandSender, String commandLabel, String[] args) {
    if (this.commandsExecutor == null) {
      return false;
    }

    return this.commandsExecutor.onCommand(commandSender, this, commandLabel, args);
  }

  public void setCommandsExecutor(CommandsExecutor commandsExecutor) {
    this.commandsExecutor = commandsExecutor;
  }
}
