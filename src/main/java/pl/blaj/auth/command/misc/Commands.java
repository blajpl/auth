package pl.blaj.auth.command.misc;

import pl.blaj.auth.command.admin.ExecutorBanList;
import pl.blaj.auth.command.admin.ExecutorKick;
import pl.blaj.auth.command.admin.ExecutorUnBan;
import pl.blaj.auth.command.player.ExecutorChangePassword;
import pl.blaj.auth.command.player.ExecutorLogin;
import pl.blaj.auth.command.player.ExecutorLogout;
import pl.blaj.auth.command.player.ExecutorRegister;
import pl.blaj.auth.command.admin.ExecutorBan;

public class Commands {

  private static Commands instance;

  public Commands() {
    instance = this;
  }

  public static Commands getInstance() {
    if (instance == null) {
      return new Commands();
    }

    return instance;
  }

  public void register() {
    new CommandsExecutor(new ExecutorLogin(), "blajessenstials.player.login", "login", true, true, 1);
    new CommandsExecutor(new ExecutorRegister(), "blajessentials.player.register", "register", true, true, 2);
    new CommandsExecutor(new ExecutorLogout(), "blajessentials.player.logout", "logout", true, true, 0);
    new CommandsExecutor(new ExecutorChangePassword(), "blajessentials.player.change-password", "change-password", true, true, 3);

    new CommandsExecutor(new ExecutorBan(), "blajessentials.admin.ban", "ban", true, true, 4);
    new CommandsExecutor(new ExecutorBanList(), "blajessentials.admin.ban-list", "ban-list", true, true, 0);
    new CommandsExecutor(new ExecutorUnBan(), "blajessentials.admin.un-ban", "unban", true, true, 1);
    new CommandsExecutor(new ExecutorKick(), "blajessentials.admin.kick", "kick", true, true, 2);
  }
}
