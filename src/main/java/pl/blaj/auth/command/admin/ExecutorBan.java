package pl.blaj.auth.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.database.model.UserBan;
import pl.blaj.auth.event.EssentialsEventHandler;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.event.user.UserBanEvent;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;
import pl.blaj.auth.util.DateTimeFormatterUtil;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ExecutorBan implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    Player playerAdmin = (Player) commandSender;
    PlayerSession adminSession =
        Auth.getInstance().getSessionManager().getPlayerSession(playerAdmin);

    if (!adminSession.isAuthorized()) {
      playerAdmin.sendMessage(ChatUtil.colored("&cKomenda tylko dla zalogowanych!"));
      return;
    }

    User adminUser = adminSession.getUser();

    String playerUsername = args[0];
    String endBanDayString = args[1];
    String endBanTimeString = args[2];
    String reason = Arrays.stream(args, 3, args.length).collect(Collectors.joining(" "));

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              try {
                User user =
                    Auth.getInstance()
                        .getDataSource()
                        .getDatabase()
                        .getUserRepository()
                        .findByUsername(playerUsername);

                if (user == null) {
                  playerAdmin.sendMessage(ChatUtil.colored("&cNie ma takiego gracza!"));
                  return;
                }

                LocalDateTime startBan = LocalDateTime.now();
                LocalDateTime endBan =
                    LocalDateTime.parse(
                        endBanDayString + " " + endBanTimeString,
                        DateTimeFormatterUtil.DEFAULT_FORMAT);

                final UserBan userBan = new UserBan();
                userBan.setReason(reason);
                userBan.setAdmin(adminUser);
                userBan.setUser(user);
                userBan.setStartBan(startBan);
                userBan.setEndBan(endBan);

                Auth.getInstance()
                    .getDataSource()
                    .getDatabase()
                    .getUserBanRepository()
                    .insert(user, userBan, adminUser);

                playerAdmin.sendMessage(ChatUtil.colored("&aZbanowałeś gracza " + playerUsername));
                Bukkit.broadcastMessage(
                    ChatUtil.colored(
                        "&aGracz "
                            + playerUsername
                            + " został zbanowany z powodu "
                            + reason
                            + " do "
                            + endBan.format(DateTimeFormatterUtil.DEFAULT_FORMAT)));

                Player player = Bukkit.getPlayer(playerUsername);

                if (player == null) {
                  return;
                }

                PlayerSession playerSession =
                    Auth.getInstance().getSessionManager().getPlayerSession(player);

                if (playerSession == null) {
                  return;
                }

                Bukkit.getScheduler()
                    .runTask(
                        Auth.getInstance(),
                        () -> {
                          player.kickPlayer(
                              ChatUtil.colored(
                                  "&cZostałeś zbanowany z powodu "
                                      + reason
                                      + " do "
                                      + endBan.format(DateTimeFormatterUtil.DEFAULT_FORMAT)));
                        });

                EssentialsEventHandler.handle(
                    new UserBanEvent(EventCause.USER, adminSession, playerSession));
              } catch (SQLException e) {
                e.printStackTrace();
              }
            });
  }
}
