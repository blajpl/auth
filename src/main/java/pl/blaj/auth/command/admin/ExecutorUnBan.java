package pl.blaj.auth.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.database.model.UserBan;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;
import pl.blaj.auth.util.DateTimeFormatterUtil;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.List;

public class ExecutorUnBan implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    Player playerAdmin = (Player) commandSender;
    PlayerSession adminSession =
        Auth.getInstance().getSessionManager().getPlayerSession(playerAdmin);

    if (!adminSession.isAuthorized()) {
      playerAdmin.sendMessage(ChatUtil.colored("&cKomenda tylko dla zalogowanych!"));
      return;
    }

    User adminUser = adminSession.getUser();

    String playerUsername = args[0];
    LocalDateTime nowDateTime = LocalDateTime.now();

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              try {
                User user =
                    Auth.getInstance()
                        .getDataSource()
                        .getDatabase()
                        .getUserRepository()
                        .findByUsername(playerUsername);

                if (user == null) {
                  playerAdmin.sendMessage(ChatUtil.colored("&cNie ma takiego gracza!"));
                  return;
                }

                UserBan userBan =
                    Auth.getInstance()
                        .getDataSource()
                        .getDatabase()
                        .getUserBanRepository()
                        .findOneByUserOnDateTime(user, nowDateTime);

                if (userBan == null) {
                  playerAdmin.sendMessage(
                      ChatUtil.colored("&cWybrany gracz nie ma aktywnego bana!"));
                  return;
                }

                userBan.setUnbanned(true);
                userBan.setUnbannedBy(adminUser);

                Auth.getInstance().getDataSource().getDatabase().getUserBanRepository().update(userBan);

                playerAdmin.sendMessage(ChatUtil.colored("&aOdbanowałeś " + user.getUsername()));
              } catch (SQLException e) {
                e.printStackTrace();
              }
            });
  }
}
