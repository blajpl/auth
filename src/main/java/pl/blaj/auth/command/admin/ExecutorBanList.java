package pl.blaj.auth.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.common.Page;
import pl.blaj.auth.database.common.Pagination;
import pl.blaj.auth.database.common.PaginationList;
import pl.blaj.auth.database.model.UserBan;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;
import pl.blaj.auth.util.DateTimeFormatterUtil;

import java.sql.SQLException;
import java.util.Optional;

public class ExecutorBanList implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    Player playerAdmin = (Player) commandSender;
    PlayerSession adminSession =
        Auth.getInstance().getSessionManager().getPlayerSession(playerAdmin);

    if (!adminSession.isAuthorized()) {
      playerAdmin.sendMessage(ChatUtil.colored("&cKomenda tylko dla zalogowanych!"));
      return;
    }

    Pagination pagination =
        Optional.ofNullable(args)
            .filter(pageNo -> pageNo.length > 0)
            .map(
                pageNo ->
                    new Pagination()
                        .setPage(
                            new Page().setPageNo(Integer.parseInt(pageNo[0])).setPageCount(10)))
            .orElse(Pagination.DEFAULT);

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              try {
                PaginationList<UserBan> userBanList =
                    Auth.getInstance()
                        .getDataSource()
                        .getDatabase()
                        .getUserBanRepository()
                        .findAllByPagination(pagination);

                int pageCount =
                    (int)
                        Math.ceil(
                            (double)
                                    (Auth.getInstance()
                                        .getDataSource()
                                        .getDatabase()
                                        .getUserBanRepository()
                                        .countAll())
                                / pagination.getPage().getPageCount());

                userBanList
                    .getItems()
                    .forEach(
                        userBan ->
                            playerAdmin.sendMessage(
                                userBan.getUser().getUsername()
                                    + " - "
                                    + userBan.getReason()
                                    + " do "
                                    + userBan
                                        .getEndBan()
                                        .format(DateTimeFormatterUtil.DEFAULT_FORMAT)
                                    + " - "
                                    + (userBan.isUnbanned() ? "Odbanowany" : "Zbanowany")));

                playerAdmin.sendMessage(
                    Math.max(userBanList.getPage().getPageNo(), 1) + " / " + pageCount);
              } catch (SQLException e) {
                e.printStackTrace();
              }
            });
  }
}
