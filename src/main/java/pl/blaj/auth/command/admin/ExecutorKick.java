package pl.blaj.auth.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;

public class ExecutorKick implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    Player playerAdmin = (Player) commandSender;
    PlayerSession adminSession =
        Auth.getInstance().getSessionManager().getPlayerSession(playerAdmin);

    if (!adminSession.isAuthorized()) {
      playerAdmin.sendMessage(ChatUtil.colored("&cKomenda tylko dla zalogowanych!"));
      return;
    }

    String playerUsername = args[0];
    String reason = args[1];

    Player player = Bukkit.getPlayer(playerUsername);

    if (player == null) {
        playerAdmin.sendMessage(ChatUtil.colored("&cNie ma takiego gracza!"));
        return;
    }

    Bukkit.getScheduler().runTask(Auth.getInstance(), () -> {
        player.kickPlayer(ChatUtil.colored("&cZostałeś wyrzucony z powodu: " + reason));
    });
  }
}
