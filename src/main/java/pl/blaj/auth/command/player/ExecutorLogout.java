package pl.blaj.auth.command.player;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.event.EssentialsEventHandler;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.event.user.UserLogoutEvent;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;

public class ExecutorLogout implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    final Player player = (Player) commandSender;
    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);

    if (!playerSession.isLogged()) {
      player.sendMessage(ChatUtil.colored("&cMusisz być zalogowany aby móc się wylogować!"));
      return;
    }

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              player.sendMessage(ChatUtil.colored("&aWylogowałeś się!"));

              Bukkit.getScheduler()
                  .runTask(
                      Auth.getInstance(),
                      () -> {
                        player.addPotionEffect(
                            new PotionEffect(PotionEffectType.BLINDNESS, Integer.MAX_VALUE, 1));
                      });

              EssentialsEventHandler.handle(new UserLogoutEvent(EventCause.USER, playerSession));
            });
  }
}
