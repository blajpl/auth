package pl.blaj.auth.command.player;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.mindrot.jbcrypt.BCrypt;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.event.EssentialsEventHandler;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.event.user.UserRegisterEvent;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;

import java.sql.SQLException;

public class ExecutorRegister implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    final Player player = (Player) commandSender;
    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);

    if (playerSession.isRegistered()) {
      player.sendMessage(ChatUtil.colored("&cJesteś już zarejestrowany!"));
      return;
    }

    String password = args[0];
    String rePassword = args[1];

    if (!password.equals(rePassword)) {
      player.sendMessage(ChatUtil.colored("&cWpisane hasła nie są identyczne!"));
      return;
    }

    if (!password.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
      player.sendMessage(
          ChatUtil.colored(
              "&cHasło musi zawierać dużą literę, znak specjalny, cyfre i mieć minimum 8 znaków!"));
      return;
    }

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              try {
                playerSession.refreshUser();
              } catch (Exception e) {
                return;
              }

              final User user = playerSession.getUser();
              final String passwordHashed = BCrypt.hashpw(password, BCrypt.gensalt());

              user.setPassword(passwordHashed);

              try {
                Auth.getInstance().getDataSource().getDatabase().getUserRepository().insert(user);
              } catch (SQLException e) {
                return;
              }

              player.sendMessage(ChatUtil.colored("&aZarejestrowałeś się pomyślnie!"));

              Bukkit.getScheduler()
                  .runTask(
                      Auth.getInstance(),
                      () -> {
                        player.removePotionEffect(PotionEffectType.BLINDNESS);
                      });

              EssentialsEventHandler.handle(new UserRegisterEvent(EventCause.USER, playerSession));
            });
  }
}
