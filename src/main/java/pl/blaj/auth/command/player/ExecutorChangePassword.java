package pl.blaj.auth.command.player;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mindrot.jbcrypt.BCrypt;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;

import java.sql.SQLException;

public class ExecutorChangePassword implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    final Player player = (Player) commandSender;
    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);

    if (!playerSession.isLogged()) {
      player.sendMessage(ChatUtil.colored("&cMusisz sie zalogować aby zmienić hasło!"));
      return;
    }

    String currentPassword = args[0];

    if (currentPassword.equals(playerSession.getUser().getPassword())) {
      player.sendMessage(ChatUtil.colored("&cWpisane aktualne hasło nie jest poprawne!"));
      return;
    }

    String newPassword = args[1];
    String reNewPassword = args[2];

    if (!newPassword.equals(reNewPassword)) {
      player.sendMessage(ChatUtil.colored("&cWpisane nowe hasła nie są identyczne!"));
      return;
    }

    if (!newPassword.matches("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{8,}$")) {
      player.sendMessage(ChatUtil.colored(
          "&cHasło musi zawierać dużą literę, znak specjalny, cyfre i mieć minimum 8 znaków!"));
      return;
    }

    final User user = playerSession.getUser();
    final boolean isValidPassword = BCrypt.checkpw(currentPassword, user.getPassword());

    if (!isValidPassword) {
      player.sendMessage(ChatUtil.colored("&cHasło jest nieprawidlowe!"));
      return;
    }

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              user.setPassword(BCrypt.hashpw(newPassword, BCrypt.gensalt()));

              try {
                Auth.getInstance()
                    .getDataSource()
                    .getDatabase()
                    .getUserRepository()
                    .updatePassword(user);
              } catch (SQLException e) {
                e.printStackTrace();
              }

              player.sendMessage(ChatUtil.colored("&aZmieniłeś swoje hasło"));
            });
  }
}
