package pl.blaj.auth.command.player;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;
import org.mindrot.jbcrypt.BCrypt;
import pl.blaj.auth.Auth;
import pl.blaj.auth.command.misc.Executor;
import pl.blaj.auth.database.model.User;
import pl.blaj.auth.event.EssentialsEventHandler;
import pl.blaj.auth.event.EventCause;
import pl.blaj.auth.event.user.UserLoginEvent;
import pl.blaj.auth.session.PlayerSession;
import pl.blaj.auth.util.ChatUtil;

public class ExecutorLogin implements Executor {

  @Override
  public void execute(CommandSender commandSender, String[] args) {
    final Player player = (Player) commandSender;
    final PlayerSession playerSession =
        Auth.getInstance().getSessionManager().getPlayerSession(player);
    final User user = playerSession.getUser();
    final String password = args[0];

    if (!playerSession.isUnauthorized()) {
      player.sendMessage(
          ChatUtil.colored("&cZalogować się można tylko jak się jest niezalogowanym!"));
      return;
    }

    if (user == null) {
      player.sendMessage(ChatUtil.colored("&cBrak konta! Zarejestruj się ponownie!"));
      return;
    }

    Bukkit.getScheduler()
        .runTaskAsynchronously(
            Auth.getInstance(),
            () -> {
              try {
                playerSession.refreshUser();
              } catch (Exception e) {
                return;
              }

              final boolean isValidPassword = BCrypt.checkpw(password, user.getPassword());

              if (!isValidPassword) {
                player.sendMessage(ChatUtil.colored("&cHasło jest nieprawidlowe!"));

                if (playerSession.getLoginAttempts() >= (int) Auth.getConfiguration().getField("config", "max-failed-login-attempts")) {
                    Bukkit.getScheduler().runTask(Auth.getInstance(), () -> {
                        player.kickPlayer(ChatUtil.colored("&cPrzekroczyłeś limit maksymalnych niepowodzeń przy logowaniu!"));
                    });

                    return;
                }

                playerSession.setLoginAttempts(playerSession.getLoginAttempts() + 1);

                return;
              }

              player.sendMessage(ChatUtil.colored("&aZalogowałeś się! Życzymy miłej gry :)"));

              Bukkit.getScheduler()
                  .runTask(
                      Auth.getInstance(),
                      () -> {
                        player.removePotionEffect(PotionEffectType.BLINDNESS);
                      });

              EssentialsEventHandler.handle(new UserLoginEvent(EventCause.USER, playerSession));
            });
  }
}
