package pl.blaj.auth.task;

import pl.blaj.auth.Auth;

public class TaskManager {

    private AfkTask afkTask;

    public TaskManager() {
        this.afkTask = new AfkTask();
    }

    public void enable() {
        this.afkTask.runTaskTimer(Auth.getInstance(), 0, 20L * 60L * Long.valueOf((int) Auth.getConfiguration().getField("config", "max-afk-time")));
    }

}
