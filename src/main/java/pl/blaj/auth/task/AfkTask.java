package pl.blaj.auth.task;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import pl.blaj.auth.Auth;
import pl.blaj.auth.util.ChatUtil;

public class AfkTask extends BukkitRunnable {

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (!player.isOnline()) {
                continue;
            }

            Location playerLocation = Auth.getInstance().getSessionManager().getActivePlayerLocations().get(player.getUniqueId());

            if (playerLocation == null) {
                Auth.getInstance().getSessionManager().getActivePlayerLocations().put(player.getUniqueId(), player.getLocation());
                continue;
            }

            if (playerLocation.equals(player.getLocation())) {
                Auth.getInstance().getSessionManager().getActivePlayerLocations().remove(player.getUniqueId());
                player.kickPlayer(ChatUtil.colored("&cZostałeś wyrzucony z powodu niekatywności!"));
            }
        }
    }

}
